package com.simpleogame.mobile.test.coolapps.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.simpleogame.mobile.test.coolapps.R;

/**
 * Created by Owner on 12/11/2016.
 */
public class UserViewHolder extends RecyclerView.ViewHolder{

    public TextView mName, mSpeciality, mArea, mPrice;
    public ImageView mPhoto;
    public RelativeLayout mContainer;

    public UserViewHolder(View itemView) {
        super(itemView);
        mName = (TextView) itemView.findViewById(R.id.item_coolapps_list_name);
        mSpeciality = (TextView) itemView.findViewById(R.id.item_coolapps_list_profession);
        mArea = (TextView) itemView.findViewById(R.id.item_coolapps_list_location);
        mPrice = (TextView) itemView.findViewById(R.id.item_coolapps_list_price);
        mPhoto = (ImageView) itemView.findViewById(R.id.item_coolapps_list_avatar);
        mContainer = (RelativeLayout) itemView.findViewById(R.id.item_coolapps_list_container);
    }
}
