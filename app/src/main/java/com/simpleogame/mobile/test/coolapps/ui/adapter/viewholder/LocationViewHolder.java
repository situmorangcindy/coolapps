package com.simpleogame.mobile.test.coolapps.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.simpleogame.mobile.test.coolapps.R;

/**
 * Created by Owner on 12/11/2016.
 */
public class LocationViewHolder extends RecyclerView.ViewHolder{

    public TextView mLocationView;

    public LocationViewHolder(View itemView) {
        super(itemView);
        mLocationView = (TextView) itemView.findViewById(R.id.item_coolapps_search_result_name);
    }
}
