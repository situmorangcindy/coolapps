package com.simpleogame.mobile.test.coolapps.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.simpleogame.mobile.test.coolapps.AppConstants;
import com.simpleogame.mobile.test.coolapps.BaseApplication;
import com.simpleogame.mobile.test.coolapps.R;
import com.simpleogame.mobile.test.coolapps.engine.json.model.LocationModel;
import com.simpleogame.mobile.test.coolapps.engine.json.parser.JSONParser;
import com.simpleogame.mobile.test.coolapps.ui.adapter.LocationAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 12/11/2016.
 */
public class SearchCoolappsActivity extends AppCompatActivity implements View.OnClickListener{
    private final static String TAG = SearchCoolappsActivity.class.getName();

    private final int REQUEST_TIMEOUT_MS = 60000;
    private JSONParser jsonParser;

    private EditText mSearchInputView;
    private RecyclerView mResultRecyclerView;

    private List<LocationModel> mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter adapter;
    private ProgressBar mProgressBar;
    private CoordinatorLayout mCoordinatorLayout;
    private LinearLayout mNoResultContainer;
    private ImageButton mSearchActionView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coolapps_search);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coolapps_coordinator_layout);
        mSearchActionView = (ImageButton) findViewById(R.id.coolapps_search_action);
        mProgressBar = (ProgressBar) findViewById(R.id.coolapps_search_result_progress_bar);
        mNoResultContainer = (LinearLayout) findViewById(R.id.coolapps_search_result_no_result_container) ;
        mSearchInputView = (EditText) findViewById(R.id.coolapps_input_search);
        mResultRecyclerView = (RecyclerView) findViewById(R.id.coolapps_search_result_recycler);
        mResultRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mResultRecyclerView.setLayoutManager(mLayoutManager);
        mSearchActionView.setOnClickListener(this);


        mSearchInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getData(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getData(final String keyword) {
        mProgressBar.setVisibility(View.VISIBLE);
        //Initializing our superheroes list
        mList = new ArrayList<LocationModel>();

        jsonParser = new JSONParser();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(AppConstants.LOCATION_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mProgressBar.setVisibility(View.INVISIBLE);
                try {
                    mList = jsonParser.getLocation(response, keyword);
                    if(mList.size() > 0) mNoResultContainer.setVisibility(View.INVISIBLE);
                    else mNoResultContainer.setVisibility(View.VISIBLE);
                    //Finally initializing our adapter
                    adapter = new LocationAdapter(mList, SearchCoolappsActivity.this);
                    //Adding adapter to recyclerview
                    mResultRecyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage(getString(R.string.unable_to_parse_server_data));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                showMessage(getString(R.string.unable_to_retrieve_data));
                mNoResultContainer.setVisibility(View.VISIBLE);
            }
        });

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to volley request queue
        BaseApplication.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void showMessage(String message) {
        if (mCoordinatorLayout == null) return;
        Snackbar snackbar = Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.coolapps_search_action:
                startActivity(new Intent(SearchCoolappsActivity.this, ListCoolappsActivity.class));
                break;
        }
    }

    public void setSelectedSearch(String keyword){
        mSearchInputView.setText(keyword);
    }
}
