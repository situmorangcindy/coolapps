package com.simpleogame.mobile.test.coolapps.ui.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.simpleogame.mobile.test.coolapps.BaseApplication;
import com.simpleogame.mobile.test.coolapps.R;
import com.simpleogame.mobile.test.coolapps.engine.json.model.UserModel;
import com.simpleogame.mobile.test.coolapps.ui.DetailCoolappsActivity;
import com.simpleogame.mobile.test.coolapps.ui.ListCoolappsActivity;
import com.simpleogame.mobile.test.coolapps.ui.adapter.viewholder.UserViewHolder;

import java.util.List;

/**
 * Created by Owner on 12/11/2016.
 */
public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    private ListCoolappsActivity context;
    List<UserModel> list;

    public UserAdapter(List<UserModel> list, ListCoolappsActivity context) {
        super();
        this.list = list;
        this.context = context;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coolapps_list, parent, false);
        UserViewHolder viewHolder = new UserViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final UserModel userModel = list.get(position);
        holder.mName.setText(userModel.getName());
        holder.mSpeciality.setText(userModel.getSpeciality());
        holder.mArea.setText(userModel.getArea());
        final String priceHtml = "<font color='#9B9B9B'> " + userModel.getCurrency() + " </font>" + "<font color='#ffffff'> " + userModel.getRate() + "</font>";
        holder.mPrice.setText(Html.fromHtml(priceHtml), TextView.BufferType.SPANNABLE);
        if (!context.mImageLoader.isInited())
            context.mImageLoader.init(ImageLoaderConfiguration.createDefault(BaseApplication.getInstance().getApplicationContext()));
        context.mImageLoader.displayImage(userModel.getPhoto(), holder.mPhoto, context.mOptionImage);
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailCoolappsActivity.class);
                intent.putExtra("id", userModel.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
