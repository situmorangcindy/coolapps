package com.simpleogame.mobile.test.coolapps.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.simpleogame.mobile.test.coolapps.AppConstants;
import com.simpleogame.mobile.test.coolapps.BaseApplication;
import com.simpleogame.mobile.test.coolapps.R;
import com.simpleogame.mobile.test.coolapps.engine.json.model.UserDetailModel;
import com.simpleogame.mobile.test.coolapps.engine.json.parser.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Owner on 12/11/2016.
 */
public class DetailCoolappsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private final static String TAG = DetailCoolappsActivity.class.getName();

    private final int REQUEST_TIMEOUT_MS = 60000;
    private JSONParser jsonParser;
    public DisplayImageOptions mOptionImage;
    public ImageLoader mImageLoader;

    private ImageView mPhotoView;
    private TextView mRecommendationView;
    private TextView mScheduleView;
    private TextView mNameView;
    private TextView mLocationView;
    private TextView mSpecialityView;
    private TextView mPriceView;
    private TextView mExperienceView;
    private TextView mDescView;
    private CoordinatorLayout mCoordinatorLayout;
    private ProgressBar mProgressBar;

    private MapView mapView;
    private GoogleMap map;

    private String id = "20";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String extId = getIntent().getStringExtra("id");

        if(extId != null){
            id = extId;
        }
        createImageOptions();
        setContentView(R.layout.activity_coolapps_detail);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coolapps_detail_coordinator_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.coolapps_detail_progress_bar);
        mPhotoView = (ImageView) findViewById(R.id.coolapps_detail_avatar);
        mRecommendationView = (TextView) findViewById(R.id.coolapps_detail_recommendation);
        mScheduleView = (TextView) findViewById(R.id.coolapps_detail_schedule);
        mNameView = (TextView) findViewById(R.id.coolapps_detail_name);
        mLocationView = (TextView) findViewById(R.id.coolapps_detail_location);
        mSpecialityView = (TextView) findViewById(R.id.coolapps_detail_profession);
        mPriceView = (TextView) findViewById(R.id.coolapps_detail_price);
        mExperienceView = (TextView) findViewById(R.id.coolapps_detail_experience);
        mDescView = (TextView) findViewById(R.id.coolapps_detail_description);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) findViewById(R.id.coolapps_detail_map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    private void createImageOptions() {
        mImageLoader = ImageLoader.getInstance();

        Resources resources = BaseApplication.getInstance().getApplicationContext().getResources();
        int circle = resources.getColor(R.color.textColorHint);
        ColorDrawable cd = new ColorDrawable(circle);

        // decode with inSampleSize
        mOptionImage = new DisplayImageOptions.Builder()
                .showImageOnLoading(cd)
                .showImageForEmptyUri(cd)
                .showImageOnFail(cd)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    private void showMessage(String message) {
        if (mCoordinatorLayout == null) return;
        Snackbar snackbar = Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void getData() {

        jsonParser = new JSONParser();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(AppConstants.PROFILE_URL + id + ".json", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mProgressBar.setVisibility(View.INVISIBLE);
                try {
                    UserDetailModel userDetailModel = jsonParser.getUserDetail(response);
                    if (!mImageLoader.isInited())
                        mImageLoader.init(ImageLoaderConfiguration.createDefault(BaseApplication.getInstance().getApplicationContext()));
                    mImageLoader.displayImage(userDetailModel.getPhoto(), mPhotoView, mOptionImage);
                    String priceHtml = "<font color='#9B9B9B'> " + userDetailModel.getCurrency() + " </font>" + "<font color='#ffffff'> " + userDetailModel.getRate() + "</font>";
                    mPriceView.setText(Html.fromHtml(priceHtml), TextView.BufferType.SPANNABLE);
                    mRecommendationView.setText(userDetailModel.getRecommendation());
                    mScheduleView.setText(userDetailModel.getSchedule());
                    mNameView.setText(userDetailModel.getName());
                    mLocationView.setText(userDetailModel.getArea());
                    mDescView.setText(userDetailModel.getDescription());
                    mExperienceView.setText(String.format("%s Years Experience", userDetailModel.getExperience()));
                    mSpecialityView.setText(userDetailModel.getSpeciality());

                    double lat = Double.parseDouble( userDetailModel.getLatitude());
                    double lng = Double.parseDouble( userDetailModel.getLongitute());LatLng latLng = new LatLng(lat,lng);
                    generateMarker(latLng);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage(getString(R.string.unable_to_parse_server_data));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                showMessage(getString(R.string.unable_to_retrieve_data));
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to volley request queue
        BaseApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (map != null) {
            return;
        }
        map = googleMap;

        if (map != null) {
            map.getUiSettings().setMyLocationButtonEnabled(false);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);

            // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
            if (isPlayServicesAvailable()) {
                MapsInitializer.initialize(DetailCoolappsActivity.this);
            }
        }
    }

    public boolean isPlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                return false;
            }
            return false;
        }
        return true;
    }


    private void generateMarker(LatLng point) {


        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting position for the marker
        markerOptions.position(point);

        // Setting title for the infowindow
        markerOptions.title(point.latitude + "," + point.longitude);

        // Adding the marker to the map
        map.addMarker(markerOptions);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(point).zoom(16).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.animateCamera(cameraUpdate);
    }
}
