package com.simpleogame.mobile.test.coolapps.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simpleogame.mobile.test.coolapps.R;
import com.simpleogame.mobile.test.coolapps.engine.json.model.LocationModel;
import com.simpleogame.mobile.test.coolapps.ui.SearchCoolappsActivity;
import com.simpleogame.mobile.test.coolapps.ui.adapter.viewholder.LocationViewHolder;

import java.util.List;

/**
 * Created by Owner on 12/11/2016.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationViewHolder> {
    private SearchCoolappsActivity context;
    List<LocationModel> list;

    public LocationAdapter(List<LocationModel> list, SearchCoolappsActivity context) {
        super();
        this.list = list;
        this.context = context;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coolapps_search_result, parent, false);
        LocationViewHolder viewHolder = new LocationViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        final LocationModel locationModel = list.get(position);
        final String categoryHtml = "<font color='#ffffff'> " + locationModel.getArea() + ", </font>" + "<font color='#FBA922'> " + locationModel.getCity() + "</font>";
        holder.mLocationView.setText(Html.fromHtml(categoryHtml), TextView.BufferType.SPANNABLE);
        holder.mLocationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.setSelectedSearch(locationModel.getArea());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
