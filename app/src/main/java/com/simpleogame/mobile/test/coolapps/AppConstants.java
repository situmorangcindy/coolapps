package com.simpleogame.mobile.test.coolapps;

/**
 * Created by Owner on 12/11/2016.
 */
public interface AppConstants {
    String BASE_URL = "http://52.76.85.10/test/";
    String LOCATION_URL = BASE_URL + "location.json";
    String DATA_URL = BASE_URL + "datalist.json";
    String PROFILE_URL = BASE_URL + "profile/";

}
