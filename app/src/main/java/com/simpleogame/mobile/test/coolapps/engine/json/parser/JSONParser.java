package com.simpleogame.mobile.test.coolapps.engine.json.parser;

import android.text.TextUtils;

import com.simpleogame.mobile.test.coolapps.engine.json.model.LocationModel;
import com.simpleogame.mobile.test.coolapps.engine.json.model.UserDetailModel;
import com.simpleogame.mobile.test.coolapps.engine.json.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Owner on 11/20/2015.
 */
public class JSONParser {

    public JSONParser() {
    }

    public List<LocationModel> getLocation(JSONArray jsonArray, String keyword) throws JSONException {
        List<LocationModel> result = new ArrayList<LocationModel>();
        if (TextUtils.isEmpty(keyword)) return result;

        if (jsonArray != null && jsonArray.length() > 0) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Iterator<?> iteratorJsonObject = jsonObject.keys();
                LocationModel locationModel = new LocationModel();
                boolean isMatch = false;
                while (iteratorJsonObject.hasNext()) {
                    String key = (String) iteratorJsonObject.next();
                    String keyValue = jsonObject.getString(key);
                    if (isMatch == false)
                        isMatch = keyValue.toLowerCase().trim().contains(keyword.toLowerCase().trim());
                    if (key.equals("area"))
                        locationModel.setArea(keyValue);
                    if (key.equals("city"))
                        locationModel.setCity(keyValue);
                }
                if (isMatch) result.add(locationModel);
            }
        }

        return result;
    }
    public List<UserModel> getUser(JSONArray jsonArray) throws JSONException {
        List<UserModel> result = new ArrayList<UserModel>();

        if (jsonArray != null && jsonArray.length() > 0) {
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Iterator<?> iteratorJsonObject = jsonObject.keys();
                UserModel userModel = new UserModel();
                while (iteratorJsonObject.hasNext()) {
                    String key = (String) iteratorJsonObject.next();
                    String keyValue = jsonObject.getString(key);
                    if (key.equals("id"))
                        userModel.setId(keyValue);
                    if (key.equals("name"))
                        userModel.setName(keyValue);
                    if (key.equals("speciality"))
                        userModel.setSpeciality(keyValue);
                    if (key.equals("area"))
                        userModel.setArea(keyValue);
                    if (key.equals("currency"))
                        userModel.setCurrency(keyValue);
                    if (key.equals("rate"))
                        userModel.setRate(keyValue);
                    if (key.equals("photo"))
                        userModel.setPhoto(keyValue);
                }
                result.add(userModel);
            }
        }

        return result;
    }
    public UserDetailModel getUserDetail(JSONObject jsonObject) throws JSONException {
        UserDetailModel userModel = new UserDetailModel();

        Iterator<?> iteratorJsonObject = jsonObject.keys();
        while (iteratorJsonObject.hasNext()) {
            String key = (String) iteratorJsonObject.next();
            String keyValue = jsonObject.getString(key);
            if (key.equals("id"))
                userModel.setId(keyValue);
            if (key.equals("name"))
                userModel.setName(keyValue);
            if (key.equals("speciality"))
                userModel.setSpeciality(keyValue);
            if (key.equals("area"))
                userModel.setArea(keyValue);
            if (key.equals("currency"))
                userModel.setCurrency(keyValue);
            if (key.equals("rate"))
                userModel.setRate(keyValue);
            if (key.equals("photo"))
                userModel.setPhoto(keyValue);
            if (key.equals("recommendation"))
                userModel.setRecommendation(keyValue);
            if (key.equals("schedule"))
                userModel.setSchedule(keyValue);
            if (key.equals("experience"))
                userModel.setExperience(keyValue);
            if (key.equals("latitude"))
                userModel.setLatitude(keyValue);
            if (key.equals("longitute"))
                userModel.setLongitute(keyValue);
            if (key.equals("description"))
                userModel.setDescription(keyValue);
        }

        return userModel;
    }
}
