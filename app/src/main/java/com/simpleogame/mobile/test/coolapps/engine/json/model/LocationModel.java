package com.simpleogame.mobile.test.coolapps.engine.json.model;

/**
 * Created by Owner on 12/11/2016.
 */
public class LocationModel {

    private String area;
    private String city;

    public LocationModel() {
    }

    public LocationModel(String area, String city) {
        this.area = area;
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
