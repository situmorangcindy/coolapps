package com.simpleogame.mobile.test.coolapps.ui;

import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.simpleogame.mobile.test.coolapps.AppConstants;
import com.simpleogame.mobile.test.coolapps.BaseApplication;
import com.simpleogame.mobile.test.coolapps.R;
import com.simpleogame.mobile.test.coolapps.engine.json.model.UserModel;
import com.simpleogame.mobile.test.coolapps.engine.json.parser.JSONParser;
import com.simpleogame.mobile.test.coolapps.ui.adapter.UserAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Owner on 12/11/2016.
 */
public class ListCoolappsActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String TAG = ListCoolappsActivity.class.getName();

    private final int REQUEST_TIMEOUT_MS = 60000;
    private JSONParser jsonParser;
    private RecyclerView mResultRecyclerView;

    private List<UserModel> mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter adapter;
    private ProgressBar mProgressBar;
    private CoordinatorLayout mCoordinatorLayout;
    private TextView mInfoPageView;
    public DisplayImageOptions mOptionImage;
    public ImageLoader mImageLoader;

    private ImageView mPreView, mNextView, mLastView, mFirstView;
    private int mTotalPage, mCurrentPage = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createImageOptions();
        setContentView(R.layout.activity_coolapps_list);
        mPreView = (ImageView) findViewById(R.id.coolapps_list_prev);
        mNextView = (ImageView) findViewById(R.id.coolapps_list_next);
        mLastView = (ImageView) findViewById(R.id.coolapps_list_last);
        mFirstView = (ImageView) findViewById(R.id.coolapps_list_first);
        mInfoPageView = (TextView) findViewById(R.id.coolapps_list_info) ;
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coolapps_list_coordinator_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.coolapps_list_progress_bar);
        mResultRecyclerView = (RecyclerView) findViewById(R.id.coolapps_list_recycler);
        mResultRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mResultRecyclerView.setLayoutManager(mLayoutManager);
        getData();

        mPreView.setOnClickListener(this);
        mNextView.setOnClickListener(this);
        mLastView.setOnClickListener(this);
        mFirstView.setOnClickListener(this);
    }

    private void getData() {
        mProgressBar.setVisibility(View.VISIBLE);
        //Initializing our superheroes list
        mList = new ArrayList<UserModel>();

        jsonParser = new JSONParser();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(AppConstants.DATA_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                mProgressBar.setVisibility(View.INVISIBLE);
                try {
                    mList = jsonParser.getUser(response);
                    mTotalPage = (int) Math.ceil(mList.size() / 10.0);
                    setAdapter();

                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage(getString(R.string.unable_to_parse_server_data));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                showMessage(getString(R.string.unable_to_retrieve_data));
            }
        });

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to volley request queue
        BaseApplication.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void showMessage(String message) {
        if (mCoordinatorLayout == null) return;
        Snackbar snackbar = Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.coolapps_list_prev:
                if (mCurrentPage > 1)
                    mCurrentPage -= 1;
                break;
            case R.id.coolapps_list_next:
                if (mCurrentPage < mTotalPage)
                    mCurrentPage += 1;
                break;
            case R.id.coolapps_list_last:
                mCurrentPage = mTotalPage;
                break;
            case R.id.coolapps_list_first:
                mCurrentPage = 1;
                break;

        }
        setAdapter();
    }

    private void createImageOptions() {
        mImageLoader = ImageLoader.getInstance();

        Resources resources = BaseApplication.getInstance().getApplicationContext().getResources();
        int circle = resources.getColor(R.color.textColorHint);
        ColorDrawable cd = new ColorDrawable(circle);

        // decode with inSampleSize
        mOptionImage = new DisplayImageOptions.Builder()
                .showImageOnLoading(cd)
                .showImageForEmptyUri(cd)
                .showImageOnFail(cd)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    private void setAdapter() {
        int start = (mCurrentPage - 1) * 10;
        int end = mCurrentPage * 10;
        if (end > mList.size()) end = mList.size();

        mInfoPageView.setText(String.format("%d - %d", start + 1, end));
        List<UserModel> list = new ArrayList<UserModel>();

        for (int i = start; i < end; i++) {
            list.add(mList.get(i));
        }
        //Finally initializing our adapter
        adapter = new UserAdapter(list, ListCoolappsActivity.this);
        //Adding adapter to recyclerview
        mResultRecyclerView.setAdapter(adapter);
    }
}
